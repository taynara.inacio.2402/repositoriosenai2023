const {createApp} =Vue;

createApp({
    data(){
        return{
            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,
        }//Fechamento return
    },//Fechamento data

    methods:{
        login(){
            // alert("Testando...");

            //Simulando uma requisição de login assincrona
            setTimeout(() => {
                if((this.usuario === "Taynara" && this.senha === "12345678") ||
                (this.usuario ==="Taynah" && this.senha === "123456")){
                    this.erro = null;
                    this.sucesso ="Login efetuado com sucesso"
                    // alert("login efetuado com sucesso!");
                }// fim do if
                else{
                    // alert("Usuário ou Senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);

        },//Fechamento login
    },//Fechamento methods

}).mount("#app");//Fechamento app