const {createApp} =Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,

            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,
        };//Fechamento return
    },//Fechamneto data
    methods:{       
        

        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                // this.valorDisplay = this.valorDisplay + numero.toString();
                //adição simplificada
                this.valorDisplay+= numero.toString();
            }
        },//Fechamento getNumero

        limpar(){
            this.valorDisplay ="0";
            this.operador = null;
            this. numeroAnterior = null;
            this. numeroAtual = null;
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){               
                this.valorDisplay += ".";
            }//fechamento if
         },//Fechamento decimal     
        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat (this.valorDisplay);
                if (this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay =  (this.numeroAtual + displayAtual).toString();
                            break; 
                        case "-":
                        this.valorDisplay = (this.numeroAtual - displayAtual) .toString();
                        break;
                    }//Fim do switch
                    this.numeroAnterior = this.numeroAtual;
                    numeroAtual = null;
                    this.numeroAtual = null;
                    this.numeroAnterior = null;

                }//Fim do if
                else{
                    this.numeroAnterior = displayAtual;
                }
            }//Fim do if numeroAtual 
            this.operador = operacao;
            this. numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay ="0";
            }
           
         },//Fim das operações

         login(){
            // alert("Testando...");

            //Simulando uma requisição de login assincrona
            setTimeout(() => {
                if((this.usuario === "Taynara" && this.senha === "12345678") ||
                (this.usuario ==="Taynah" && this.senha === "123456") ||
                (this.usuario === "Tayan" && this.senha === "12345") ||
                (this.usuario === "Taynan" && this.senha === "1234")
                ){
                    this.erro = null;
                    this.sucesso ="Login efetuado com sucesso!";
                    window.location.href='calculadora.html';
                    // alert("login efetuado com sucesso!");
                }// fim do if
                else{
                    // alert("Usuário ou Senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);
        },
    }//Fechamento methods

}).mount("#app");//Fechamento app