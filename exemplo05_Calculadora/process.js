const {createApp} =Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
        };//Fechamento return
    },//Fechamneto data
    methods:{       
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                // this.valorDisplay = this.valorDisplay + numero.toString();
                //adição simplificada
                this.valorDisplay+= numero.toString();
            }
        },//Fechamento getNumero

        limpar(){
            this.valorDisplay ="0";
            this.operador = null;
            this. numeroAnterior = null;
            this. numeroAtual = null;
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){               
                this.valorDisplay += ".";
            }//fechamento if
         },//Fechamento decimal     
         operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat (this.valorDisplay);
                if (this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay =  (this.numeroAtual + displayAtual).toString();
                            break; 
                        case "-":
                        this.valorDisplay = (this.numeroAtual - displayAtual) .toString();
                        break;
                    }//Fim do switch
                    this.numeroAnterior = this.numeroAtual;
                    numeroAtual = null;
                    this.numeroAtual = null;
                    this.numeroAnterior = null;

                }//Fim do if
                else{
                    this.numeroAnterior = displayAtual;
                }
            }//Fim do if numeroAtual 
            this.operador = operacao;
            this. numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay ="0";
            }
           
         }//Fim das operações
    }//Fechamento methods

}).mount("#app");//Fechamento app