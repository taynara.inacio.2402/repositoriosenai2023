const {createApp} = Vue; // Importa a função createApp do Vue.js

createApp({
    data(){
        return{
            valorDisplay:"0", // Valor exibido no display da calculadora
            operador:null, // Operador atualmente selecionado
            numeroAtual:null, // Número atualmente em uso
            numeroAnterior:null, // Número anteriormente utilizado
            tamanhoLetra: 50 + "px", // Tamanho da letra no display

            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,

            //Arrays (vetores) para armazenamento dos nomes de usuários e senhas
            usuarios: ["admin", "taynara", "taynan"],
            senhas:["1234", "1234", "1234"],
            userAdmin: false,
            mostrarEntrada: false,

            //Variáveis para tratamento das informações dos novos usuários
            newUsername: "",
            newPassword: "",
            confirmPassword: "",
        
            mostrarLista: false,
        };
    },

    methods:{
        getNumero(numero){
            this.ajusteTamanhoDisplay(); // Chama a função para ajustar o tamanho da letra no display

            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString(); // Se o valor exibido for 0, substitui pelo número digitado
            }
            else{
            /*     Analisa de o operador é igual a "=" para que após uma operação, o valor numérico acionado não seja concatenado ao valor do display*/
                if(this.operador == "="){
                    this.valorDisplay = "";
                    this.operador = null;
                }

                this.valorDisplay += numero.toString(); // Concatena o número digitado ao valor exibido
            }
        },//Fechamento getNumero
        
        limpar(){
            this.valorDisplay = "0"; // Limpa o valor exibido
            this.operador = null; // Remove o operador selecionado
            this.numeroAnterior = null; // Remove o número anteriormente utilizado
            this.numeroAtual = null; // Remove o número atualmente em uso
            this.tamanhoLetra = 50 + "px"; // Restaura o tamanho padrão da letra no display
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += "."; // Adiciona um ponto decimal se ainda não estiver presente
            }
        },//Fechamento decimal

        operacoes(operacao){
            /*A função é responsável por executar operações matemáticas com base em um operador selecionado.*/

            // Verifica se this.numeroAtual não é nulo.
            if(this.numeroAtual != null){
                /*Converte o valor do display atual (this.valorDisplay) para um número de ponto flutuante.*/
                const displayAtual = parseFloat(this.valorDisplay);

                if(this.operador != null){
                    /* Executa a operação com base no operador selecionado. A operação é realizada entre this.numeroAtual (valor anterior) e displayAtual (valor do display).*/
                    switch(this.operador){
                        case "+":
                            // Caso o operador seja "+", adiciona os dois valores e armazena o resultado em this.valorDisplay.
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                        
                        case "-":
                            // Caso o operador seja "-", subtrai displayAtual de this.numeroAtual e armazena o resultado em this.valorDisplay.
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;
                        
                        case "*":
                            /*Caso o operador seja "*", multiplica this.numeroAtual por displayAtual e armazena o resultado em this.valorDisplay.*/
                            this.valorDisplay = (this.numeroAtual * displayAtual).toString();
                            break;

                        case "/":
                            /*Caso o operador seja "/", verifica se displayAtual ou this.numeroAtual é igual a zero. Se for o caso, define this.valorDisplay como "Operação impossível!". Caso contrário, realiza a divisão entre this.numeroAtual e displayAtual e armazena o resultado em this.valorDisplay.*/
                            if(displayAtual == 0 || this.numeroAtual == 0){
                                this.valorDisplay = "Operação impossível!";
                            }
                            else{
                                this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                            }
                            break;
                    }

                    // Atualiza this.numeroAnterior para this.numeroAtual e define this.numeroAtual como nulo.
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;

                    // Se o operador for diferente de "=", define this.operador como nulo.                    
                    if(this.operador != "="){
                        this.operador = null;
                    }
                    
                    /*Verifica se o valor do display atual (this.valorDisplay) contém um ponto decimal. Se sim, converte o valor para um número de ponto flutuante (numDecimal) e redefine this.valorDisplay como uma string contendo o valor formatado com duas casas decimais.*/
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay = (numDecimal.toFixed(2)).toString();
                    }                    
                }
                else{
                    // Caso contrário, define this.numeroAnterior como displayAtual.
                    this.numeroAnterior = displayAtual;
                }
            }//Fechamento if

            // Define this.operador como operacao.
            this.operador = operacao;

            /*Converte o valor do display atual (this.valorDisplay) para um número de ponto flutuante e armazena em this.numeroAtual.*/
            this.numeroAtual = parseFloat(this.valorDisplay);

            // Se o operador for diferente de "=", redefine this.valorDisplay como "0".
            if(this.operador != "="){
                this.valorDisplay = "0";
            }

            this.ajusteTamanhoDisplay(); // Chama a função para ajustar o tamanho da letra no display
        },//Fechamento operações
        
        ajusteTamanhoDisplay(){
            // Ajusta o tamanho da letra no display com base no comprimento do número exibido
            if(this.valorDisplay.length >= 31){
                this.tamanhoLetra = 14 + "px"; // Se o número exibido for muito longo, diminui o tamanho da letra
                return;                
            }
            else if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 20 + "px"; // Se o número exibido for longo, diminui o tamanho da letra
                return;
            }
            else if(this.valorDisplay.length >= 12){
                this.tamanhoLetra = 30 + "px"; // Se o número exibido for médio, diminui o tamanho da letra
            }
            else{
                this.tamanhoLetra = 50 + "px"; // Caso contrário, usa o tamanho padrão da letra
            }
        },//Fechamento ajusteTamanhoDisplay

        login(){
            // alert("Testando...");

            //Simulando uma requisão de login assincrona
            setTimeout(() => {
                if((this.usuario === "Adriano" && this.senha === "12345678") || 
                (this.usuario === "Julia" && this.senha === "123456")){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    window.location.href='calculadora.html';
                    
                    // alert("Login efetuado com sucesso!");
                }//Fim do if
                else{
                    // alert("Usuário ou Senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);

        }, //Fechamento login

        login2(){
            this.mostrarEntrada = false;

            setTimeout(() => {
                this.mostrarEntrada = true;

                //Verificação de usuário e senha cadastrados ou não nos arrays
                const index = this.usuarios.indexOf(this.usuario);
                if(index !== -1 && this.senhas[index] === this.senha){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    //Registrando o usuário no LocalStorage para lembrete de acessos
                    localStorage.setItem("usuario", this.usuario);
                    localStorage.setItem("senha", this.senha);

                    //Verificando se o usuário é admin
                    if(this.usuario === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!";
                    }
                }//Fechamento if
                else{
                    this.sucesso = null;
                    this.erro = "Usuário e / ou senha incorretos!";
                }      
            }, 1000);            
        },//Fechamento login2

        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.erro = null;
                this.sucesso = "Carregando Página de Cadastro...";
                this.mostrarEntrada = true;

                //Espera estratégica antes do carregamento da página
                setTimeout(() => {}, 2000);

                setTimeout(() => {
                    window.location.href = "cadastro.html";
                }, 1000);                
            }//Fechamento if
            else{
                this.sucesso = null;
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.erro = "Sem privilégios ADMIN!!!";
                }, 1000);
            }//Fechamento else

        },//Fechamento paginaCadastro

        adicionarUsuario(){
            this.mostrarEntrada = false;

            this.usuario = localStorage.getItem("usuario");
            this.senha = localStorage.getItem("senha");

            setTimeout(() => {
                this.mostrarEntrada = true;

                if(this.usuario === "admin"){
                    /*Verificando de o novo usuário é diferente de vazio e se ele já não está cadastrado no sistema*/
                    if(!this.usuarios.includes(this.newUsername) && this.newUsername!== "" && !this.newUsername.includes(" ")){
                        //Validação da senha digitada para cadastro no array
                        if(this.newPassword !== "" && !this.newPassword.includes(" ") && this.newPassword === this.confirmPassword){
                            // Inserindo o novo usuário e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas. push(this.newPassword);
                             
                            // Atualizendo o usuário recém cadastrado no LOCALSTORA
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                            this.erro = null;
                            this.sucesso = "Usuário cadastrado com sucesso!";
                        }// Fechamento if password
                        else{
                            this.sucesso = null;
                            this.erro = "Por favor, informe uma senha válida!!!";

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                        }//Fechamento else
                     }//Fechamento if  includes
                    else{
                        this.erro = "Usuário invalido! por favor digite um Usuário diferente!"
                        this.sucesso =null;

                        this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";
                    }//Fechamento else

                }//Fechamento if admin
                
                else{
                    this.erro = " Não está logado como ADMIN!";
                    this.sucesso = null;

                    this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                }//Fechamento else admin
            }, 300);

        },//Fechamento adicionarUsuario
  
        listarUsuario(){
            
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem ("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;

        },//Fechamento listarUsuario

        excluirusuario(usuario){
            this.mostrarEntrada = false;
            if(this.usuario === "admin"){
             setTimeout(() => {
                this.mostrarEntrada = true;
                this.sucesso = null;
                this.erro = "O usuário ADMIN não pode ser excluído!!!";
             }, 500);
             return; //Forçaa saída deste bloco
            }//fechamento do if

            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index  = this.usuarios.indexOf(usuario);
                if (index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    //Atualiza array usuarios no LOCALstorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));
                    setTimeout(() => {
                        this.mostrarEntrada = true;
                        this.erro =null;
                        this.sucesso = " Usuário excluído com sucesso!";
                    }, 500);
                }//Fechamento if index
            }//Fechamento if
        },//Fechamento excluirUsuario

    },//Fechamento methods

}).mount("#app"); // Monta a aplicação Vue.js no elemento HTML com o ID "app"